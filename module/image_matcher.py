# -*- coding: utf-8 -*-
# @Time           : 2018/6/10 15:35
# @Author         : Cirno
# @File           : image_matcher.py
# @Description    ：
import numpy as np
import cv2


def matcher(img_page, icon_path, threshold=0.8):
    img_page_np = np.array(img_page)
    img_page_gray = cv2.cvtColor(img_page_np, cv2.COLOR_BGR2GRAY)
    icon = cv2.imread(icon_path, 0)
    height, width = icon.shape
    res = cv2.matchTemplate(img_page_gray, icon, cv2.TM_CCOEFF_NORMED)
    loc = np.where(res >= threshold)
    # debug area
    # for pt in zip(*loc[::-1]):
    #     cv2.rectangle(img_page_gray, pt, (pt[0] + width, pt[1] + height), (255, 255, 255.0), 2)
    # cv2.imshow('win', img_page_gray)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    # debug area
    for x, y in zip(*loc[::-1]):
        return x, y
    return None, None
