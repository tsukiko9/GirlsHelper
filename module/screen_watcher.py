# -*- coding: utf-8 -*-
# @Time           : 2018/6/10 12:48
# @Author         : Cirno
# @File           : screen_watcher.py
# @Description    ：screen watcher
import win32gui
import win32ui
from ctypes import windll
from PIL import Image
from module.cmd_logger import logger
from define.global_define import EnumMsgType

import win32con

logger_types = EnumMsgType


def screen_watcher(event=None):
    # powered by https://stackoverflow.com/questions/19695214/python-screenshot-of-inactive-window-printwindow-win32gui
    hwnd = win32gui.FindWindow(None, "雷电模拟器")

    left, top, right, bot = win32gui.GetWindowRect(hwnd)
    w = right - left
    h = bot - top

    hwnd_dc = win32gui.GetWindowDC(hwnd)
    mfc_dc = win32ui.CreateDCFromHandle(hwnd_dc)
    save_dc = mfc_dc.CreateCompatibleDC()

    save_bit_map = win32ui.CreateBitmap()
    save_bit_map.CreateCompatibleBitmap(mfc_dc, w, h)
    save_dc.SelectObject(save_bit_map)

    img_dc = mfc_dc
    mem_dc = save_dc
    mem_dc.BitBlt((0, 0), (w, h), img_dc, (100, 100), win32con.SRCCOPY)

    if windll.user32.PrintWindow(hwnd, save_dc.GetSafeHdc(), 0) == 1:
        logger("事件：【" + event + "】，截图成功...", logger_types.Success)
    else:
        logger("事件：【" + event + "】，截图失败", logger_types.Error)
        return None

    bmp_info = save_bit_map.GetInfo()
    bmp_str = save_bit_map.GetBitmapBits(True)

    # generate an image
    img = Image.frombuffer(
        'RGB',
        (bmp_info['bmWidth'], bmp_info['bmHeight']),
        bmp_str, 'raw', 'BGRX', 0, 1)

    # memory release
    win32gui.DeleteObject(save_bit_map.GetHandle())
    save_dc.DeleteDC()
    mfc_dc.DeleteDC()
    win32gui.ReleaseDC(hwnd, hwnd_dc)

    return img
