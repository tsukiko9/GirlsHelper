# -*- coding: utf-8 -*-
# @Time           : 2018/6/10 14:57
# @Author         : Cirno
# @File           : cmd_logger.py
# @Description    ：
import time

from define.global_define import EnumMsgType

logger_type = EnumMsgType


def logger(msg, msg_type):
    this_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
    if msg_type == logger_type.Error:
        print(this_time + " - [Error] : " + msg)
    elif msg_type == logger_type.Success:
        print(this_time + " - [Success] : " + msg)
    elif msg_type == logger_type.Info:
        print(this_time + " - [Info] : " + msg)
    elif msg_type == logger_type.Warning:
        print(this_time + " - [Waring] : " + msg)
