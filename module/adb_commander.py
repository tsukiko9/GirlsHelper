# -*- coding: utf-8 -*-
# @Time           : 2018/6/10 14:09
# @Author         : Cirno
# @File           : adb_commander.py
# @Description    ：adb commander line
# @Parameter      : mode type 0: use adb; 1:dl
import os

from conf import global_conf
from define import global_define
from module import cmd_logger

logger_type = global_define.EnumMsgType


def adb_commander(cmd, mode=0):
    conf = global_conf.GlobalConf
    if mode == 0:
        cmd_path = conf.adb_path + " shell"
    elif mode == 1:
        cmd_path = conf.dl_path
    else:
        cmd_logger.logger("adb cmd mode type error...", logger_type.Error)
        return None
    cmd_line = cmd_path + " " + cmd
    os.popen(cmd_line)
    cmd_logger.logger("cmd line : " + cmd + " executed...", logger_type.Info)
