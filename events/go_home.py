# -*- coding: utf-8 -*-
# @Time           : 2018/6/10 22:08
# @Author         : Cirno
# @File           : go_home.py
# @Description    ：
import time

from module.screen_watcher import screen_watcher
from module.image_matcher import matcher
from module.adb_commander import adb_commander
from module.cmd_logger import logger

from define.enum_icon_home import EnumIconHome
from define.global_define import EnumMsgType, EnumIconGlobal

EVENT_NAME = "返回首页"
home_icon = EnumIconHome
global_icon = EnumIconGlobal
logger_type = EnumMsgType


def go_home():
    while 1:
        # 返回操作
        page_np = screen_watcher(EVENT_NAME)
        icon_back = global_icon.icon_global_back
        (x, y) = matcher(page_np, icon_back.value)
        if (x, y) == (None, None):
            logger("事件：【" + EVENT_NAME + "】识别失败..当前可能页面不存在返回键..请打开调试模式..", logger_type.Error)
            return None
        else:
            logger("事件：【" + EVENT_NAME + "】识别成功..延时8s..", logger_type.Error)
            cmd = "input tap " + str(x) + " " + str(y)
            adb_commander(cmd)
            time.sleep(8)

        # 检测是否返回首页
        # page_np = screen_watcher(EVENT_NAME)
        icon_home_go_fight = home_icon.icon_home_go_fight_normal
        (x, y) = matcher(page_np, icon_home_go_fight.value)
        if (x, y) == (None, None):
            logger("事件：【" + EVENT_NAME + "】未退回首页，继续执行..", logger_type.Error)
            continue
        else:
            logger("事件：【" + EVENT_NAME + "】回到首页成功..延时5s..", logger_type.Error)
            time.sleep(5)
            break
