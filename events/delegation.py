# -*- coding: utf-8 -*-
# @Time           : 2018/6/10 16:59
# @Author         : Cirno
# @File           : delegation.py
# @Description    ：
import time

from module.screen_watcher import screen_watcher
from module.image_matcher import matcher
from module.adb_commander import adb_commander
from module.cmd_logger import logger

from define.enum_icon_home import EnumIconHome
from define.global_define import EnumMsgType
from events.go_home import go_home

EVENT_NAME = "委托"
home_icon = EnumIconHome
logger_type = EnumMsgType


def delegation_task():
    # 0 - 跳转为HOME页
    go_home()

    # 1 - 当前为HOME页，打开左侧委托面板
    page_np = screen_watcher(EVENT_NAME)
    home_left_panel = home_icon.icon_home_left_panel_red
    (x, y) = matcher(page_np, home_left_panel.value)
    if (x, y) == (None, None):
        logger("事件：【" + EVENT_NAME + "】主页委托面板识别失败..", logger_type.Error)
        return None
    else:
        logger("事件：【" + EVENT_NAME + "】主页委托面板识别成功..延时5s..", logger_type.Error)
        cmd = "input tap " + str(x) + " " + str(y)
        adb_commander(cmd)
        time.sleep(5)

    # 2 - 委托完成
    page_np = screen_watcher(EVENT_NAME)
    home_left_panel = home_icon.icon_home_left_panel_btn_red
    (x, y) = matcher(page_np, home_left_panel.value)
    if (x, y) == (None, None):
        logger("事件：【" + EVENT_NAME + "】委托完成按钮识别失败..", logger_type.Error)
        return None
    else:
        logger("事件：【" + EVENT_NAME + "】委托完成按钮识别成功..延时5s..", logger_type.Error)
        cmd = "input tap " + str(x) + " " + str(y)
        adb_commander(cmd)
        time.sleep(5)