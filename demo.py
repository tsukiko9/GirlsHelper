# encoding: utf-8
"""
@Author: Baka9 

@Version: 1.0
@File: demo.py
@Time: 2018/6/9 14:36
@Description: Launcher demo
"""
import numpy as np
import cv2
import ctypes
import os
import win32gui
import win32ui
import win32con
from ctypes import windll
from PIL import Image


class RECT(ctypes.Structure):
    _fields_ = [('left', ctypes.c_long), ('top', ctypes.c_long), ('right', ctypes.c_long), ('bottom', ctypes.c_long)]

    def __str__(self):
        return str((self.left, self.top, self.right, self.bottom))


# if __name__ == '__main__':
#
#     # 对后台应用程序截图，程序窗口可以被覆盖，但如果最小化后只能截取到标题栏、菜单栏等。
#
#
#     # 获取要截取窗口的句柄
#     hwnd = win32gui.FindWindow(None, "雷电模拟器")
#
#     # 获取句柄窗口的大小信息
#     # 可以通过修改该位置实现自定义大小截图
#     left, top, right, bot = win32gui.GetWindowRect(hwnd)
#     w = right - left
#     h = bot - top
#
#     # 返回句柄窗口的设备环境、覆盖整个窗口，包括非客户区，标题栏，菜单，边框
#     hwndDC = win32gui.GetWindowDC(hwnd)
#
#     # 创建设备描述表
#     mfcDC = win32ui.CreateDCFromHandle(hwndDC)
#
#     # 创建内存设备描述表
#     saveDC = mfcDC.CreateCompatibleDC()
#
#     # 创建位图对象
#     saveBitMap = win32ui.CreateBitmap()
#     saveBitMap.CreateCompatibleBitmap(mfcDC, w, h)
#     saveDC.SelectObject(saveBitMap)
#
#     # 截图至内存设备描述表
#     img_dc = mfcDC
#     mem_dc = saveDC
#     mem_dc.BitBlt((0, 0), (w, h), img_dc, (100, 100), win32con.SRCCOPY)
#
#     # 将截图保存到文件中
#     # saveBitMap.SaveBitmapFile(mem_dc, 'screenshot.bmp')
#
#     # 改变下行决定是否截图整个窗口，可以自己测试下
#     # result = windll.user32.PrintWindow(hwnd, saveDC.GetSafeHdc(), 1)
#     result = windll.user32.PrintWindow(hwnd, saveDC.GetSafeHdc(), 0)
#     print(result)
#
#     # 获取位图信息
#     bmpinfo = saveBitMap.GetInfo()
#     bmpstr = saveBitMap.GetBitmapBits(True)
#     # 生成图像
#     im = Image.frombuffer(
#         'RGB',
#         (bmpinfo['bmWidth'], bmpinfo['bmHeight']),
#         bmpstr, 'raw', 'BGRX', 0, 1)
#
#     # 内存释放
#     win32gui.DeleteObject(saveBitMap.GetHandle())
#     saveDC.DeleteDC()
#     mfcDC.DeleteDC()
#     win32gui.ReleaseDC(hwnd, hwndDC)
#
#     # 存储截图
#     # if result == 1:
#     #     # PrintWindow Succeeded
#     #     im.save("test.png")
#     #     # im.show()
#
#     # do something
#     img_page_np = np.array(im)
#     img_page_gray = cv2.cvtColor(img_page_np, cv2.COLOR_BGR2GRAY)
#     btn_go_fight = cv2.imread("resource" + r"\b" + "utton\icon_go_fight.png", 0)
#     height, width = btn_go_fight.shape
#     res = cv2.matchTemplate(img_page_gray, btn_go_fight, cv2.TM_CCOEFF_NORMED)
#     threshold = 0.8
#     loc = np.where(res >= threshold)
#     adb_path = '''"D:\Program Files\dnplayer2\ld"'''
#     adb_path2 = '''D:\Soft\platform-tools''' + r'''\a''' + '''db shell'''
#     for x, y in zip(*loc[::-1]):
#         cmd = str(adb_path2 + " input tap " + str(x) + " " + str(y))
#         adb_shell = os.popen(cmd)
#
#         print(x, y)
#         print(cmd)
#         break
#     # for pt in zip(*loc[::-1]):
#     #     cv2.rectangle(img_page_gray, pt, (pt[0] + width, pt[1] + height), (255, 255, 255.0), 2)
#     # cv2.imshow('win', img_page_gray)
#     # cv2.waitKey(0)
#     # cv2.destroyAllWindows()
#
#     pass
    # demo - 1 font window
    # rect = RECT()
    #
    # title_name = "雷电模拟器"
    # hwnd = win32gui.FindWindow(None, title_name)
    # left, top, right, bottom = win32gui.GetWindowRect(hwnd)
    # print(left, top, right, bottom)
    # ctypes.windll.user32.GetWindowRect(hwnd, ctypes.byref(rect))
    # rangle = (rect.left + 2, rect.top - 2, rect.right - 2, rect.bottom - 2)
    # pic = ImageGrab.grab(rangle)
    #
    # img_np = np.array(pic)
    # RGB_img = cv2.cvtColor(img_np, cv2.COLOR_BGR2RGB)
    # cv2.imshow('win', RGB_img)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
    # pass

    # demo - cv2 usage
    # page_main = cv2.imread("test.png")
    # img_page_gray = cv2.cvtColor(page_main, cv2.COLOR_BGR2GRAY)
    # btn_go_fight = cv2.imread("icon_home_go_fight_normal.png", 0)
    # height, width = btn_go_fight.shape
    # res = cv2.matchTemplate(img_page_gray, btn_go_fight, cv2.TM_CCOEFF_NORMED)
    # threshold = 0.8
    # loc = np.where(res >= threshold)
    #
    # for pt in zip(*loc[::-1]):
    #     cv2.rectangle(img_page_gray, pt, (pt[0] + width, pt[1] + height), (255, 255, 255.0), 2)
    # cv2.imshow('win', img_page_gray)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()
