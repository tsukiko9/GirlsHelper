# -*- coding: utf-8 -*-
# @Time           : 2018/6/10 15:15
# @Author         : Cirno
# @File           : global_define.py
# @Description    ：

from enum import Enum


class EnumMsgType(Enum):
    Error = -1
    Success = 0
    Info = 1
    Warning = 2


class EnumIconGlobal(Enum):
    icon_global_back = "resource" + r"\button\icon_global_back.png"
