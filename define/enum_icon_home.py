# -*- coding: utf-8 -*-
# @Time           : 2018/6/10 19:30
# @Author         : Cirno
# @File           : enum_icon_home.py
# @Description    ：
from enum import Enum


class EnumIconHome(Enum):
    icon_home_go_fight_normal = "resource" + r"\button\icon_home_go_fight_normal.png"
    icon_home_left_panel_red = "resource" + r"\button\icon_home_left_panel_red.png"
    icon_home_left_panel_normal = "resource" + r"\button\icon_home_left_panel_normal.png"
    icon_home_left_panel_btn_red = "resource" + r"\button\icon_home_left_panel_btn_red.png"
