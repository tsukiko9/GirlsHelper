# -*- coding: utf-8 -*-
# @Time           : 2018/6/10 12:36
# @Author         : Cirno
# @File           : conf_global.py
# @Description    ：
# @Parameter      : adb_path    : abd or all path
#                 : dl_path     : only 雷神模拟器


class GlobalConf:

    adb_path = "D:\Soft\platform-tools" + r"\adb"
    dl_path = """D:\Program Files\dnplayer2""" + r"\dl"
